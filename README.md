# Bootstrap4 Theme

Tema básico para sistemas da EESC. Trocando esse tema é possível trocar o layout do sistema.

## Como usar

De forma simplificada você coloca como dependência no seu composer.json este repositório e mais alguns necessários. Como este repositório não está público é necessário configurar a url para baixar.

Veja o repositório https://gitlab.uspdigital.usp.br/eesc/sistema-padrao para saber como utilizar este tema.


## O que vem?

Este tema é baseado no boostrap 4 bundle. Além dele já está configurado o jquery, fontawesome e datatables.

O sistema de templates é o raelgc/template. É um engine bem simples e fácil de usar.

Este tema já coloca uma linha no topo para o nome da aplicação e o logo da EESC.

Depois uma linha com as opções do usuário como login e logout.

Na sequência tem uma barra de menus que foi feito para pertencer à aplicação propriamente.

No final tem um rodapé padrão para a EESC.
